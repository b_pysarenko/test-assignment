package com.gemicle.skf.services.impl;

import com.gemicle.skf.entities.Message;
import com.gemicle.skf.repositories.MessageRepository;
import com.gemicle.skf.repositories.impl.CustomTimestampRepositoryImpl;
import com.gemicle.skf.services.MessagingService;
import org.springframework.stereotype.Service;

import java.time.ZonedDateTime;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class MessagingServiceImpl implements MessagingService {

    private final MessageRepository repository;
    private final CustomTimestampRepositoryImpl timestampRepository;

    public MessagingServiceImpl(
            MessageRepository repository,
            CustomTimestampRepositoryImpl timestampRepository) {
        this.repository = repository;
        this.timestampRepository = timestampRepository;
    }

    @Override
    public Message publishMessage(Object content) {
        Message msg = repository.save(new Message(content));
        timestampRepository.save(msg.getId(),msg.getCreateDate().toInstant().toEpochMilli());
        return msg;
    }

    @Override
    public Message getLastMessage() {
        return repository.findById(timestampRepository.getMax()).get();
    }

    @Override
    public List<Message> getMessagesByPeriod(ZonedDateTime start, ZonedDateTime end) {
        Collection<String> ids = timestampRepository.getInRangeByScore(
                start.toInstant().toEpochMilli(), end.toInstant().toEpochMilli());
        if (Objects.isNull(ids) || ids.isEmpty()) {
            return new ArrayList<>();
        }
        return StreamSupport.stream(repository.findAllById(ids).spliterator(), false)
                .sorted(Comparator.comparing(Message::getCreateDate))
                .collect(Collectors.toList());
    }
}
