package com.gemicle.skf.services;

import com.gemicle.skf.entities.Message;

import java.time.ZonedDateTime;
import java.util.List;

public interface MessagingService {

    Message publishMessage(Object content);

    Message getLastMessage();

    List<Message> getMessagesByPeriod(ZonedDateTime start, ZonedDateTime end);
}
