package com.gemicle.skf.exceptions;

public class NoDataPresentException extends RuntimeException{

    public NoDataPresentException(String msg) {
        super(msg);
    }
}
