package com.gemicle.skf.entities;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@RedisHash(Message.MESSAGE_KEY)
public class Message {
    public static final String MESSAGE_KEY = "msg";
    @Id
    private String id;
    private Object content;
    private ZonedDateTime createDate;

    public Message(Object content) {
        this.createDate = ZonedDateTime.now(ZoneOffset.UTC);
        this.content = content;
    }
}
