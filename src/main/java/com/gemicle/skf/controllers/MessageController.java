package com.gemicle.skf.controllers;

import com.gemicle.skf.entities.Message;
import com.gemicle.skf.services.MessagingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.List;

@RestController
@RequestMapping("/messages")
public class MessageController {

    private static final String DATE_TIME_PATTERN = "yyyy-MM-dd HH:mm:ss";

    @Autowired
    private MessagingService messagingService;

    @PostMapping("/publish")
    Message publishMessage(@RequestBody String content){
        return messagingService.publishMessage(content);
    }

    @GetMapping("/getLast")
    Message getLastMessage(){
        return messagingService.getLastMessage();
    }

    @GetMapping("/getByTime")
    List<Message> getMessagesByTime(@RequestParam(name = "start")
                                    @DateTimeFormat(pattern = DATE_TIME_PATTERN) LocalDateTime startTime,
                                    @RequestParam(name = "end")
                                    @DateTimeFormat(pattern = DATE_TIME_PATTERN) LocalDateTime endTime){
        return messagingService.getMessagesByPeriod(
                startTime.atZone(ZoneOffset.UTC), endTime.atZone(ZoneOffset.UTC));
    }
}
