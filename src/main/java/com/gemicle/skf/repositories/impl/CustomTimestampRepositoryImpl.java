package com.gemicle.skf.repositories.impl;

import com.gemicle.skf.exceptions.NoDataPresentException;
import com.gemicle.skf.repositories.CustomSortedRepository;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ZSetOperations;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.Set;


@Repository
public class CustomTimestampRepositoryImpl implements CustomSortedRepository<String, Long> {

    private static final String TIMESTAMP_KEY = "ts";

    private final RedisTemplate<String, String> template;
    private final ZSetOperations<String, String> zSetOperations;

    public CustomTimestampRepositoryImpl(RedisTemplate<String, String> template){
        this.template = template;
        this.zSetOperations = template.opsForZSet();
    }

    @Override
    public boolean save(String id, Long timestamp) {
        return zSetOperations.add(TIMESTAMP_KEY, id, timestamp);
    }

    @Override
    public String getMax() {
        return Optional.ofNullable(zSetOperations.reverseRange(TIMESTAMP_KEY, 0, 0))
                .orElseThrow(() -> new NoDataPresentException("Timestamp elements not present"))
                .stream().findFirst()
                .orElseThrow(() -> new NoDataPresentException("Timestamp elements not present"));
    }

    @Override
    public Set<String> getInRangeByScore(Long start, Long end) {
        return zSetOperations.rangeByScore(TIMESTAMP_KEY, start, end);
    }
}
