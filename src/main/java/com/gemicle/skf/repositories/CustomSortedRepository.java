package com.gemicle.skf.repositories;

import org.springframework.data.repository.NoRepositoryBean;

import java.util.Collection;
import java.util.Set;

@NoRepositoryBean
public interface CustomSortedRepository<Value, Score> {

    boolean save(Value value, Score score);

    Value getMax();

    Set<Value> getInRangeByScore(Score bottom, Score top);

}
