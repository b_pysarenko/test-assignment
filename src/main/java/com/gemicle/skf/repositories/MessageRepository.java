package com.gemicle.skf.repositories;

import com.gemicle.skf.entities.Message;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface MessageRepository extends CrudRepository<Message, String> {

}
