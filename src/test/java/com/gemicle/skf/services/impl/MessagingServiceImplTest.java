package com.gemicle.skf.services.impl;

import com.gemicle.skf.config.RedisTestConfig;
import com.gemicle.skf.entities.Message;
import com.gemicle.skf.repositories.MessageRepository;
import com.gemicle.skf.repositories.impl.CustomTimestampRepositoryImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.ZonedDateTime;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@ContextConfiguration( classes = RedisTestConfig.class )
@SpringBootTest
@TestPropertySource("classpath:application-test.yml")
@ActiveProfiles("test")
class MessagingServiceImplTest {

    @Autowired
    RedisTemplate<String,String> template;
    @Autowired
    MessageRepository repository;
    @Autowired
    CustomTimestampRepositoryImpl tsRepository;

    MessagingServiceImpl service;

    @BeforeEach
    void setUp() {
        template.execute((RedisCallback<Void>) connection -> {
            connection.flushDb();
            return null;
        });
        service = new MessagingServiceImpl(repository,tsRepository);
    }

    @Test
    void publishMessageTest() {
        Message msg = service.publishMessage("test");
        String content = (String)repository.findById(msg.getId()).get().getContent();
        assertEquals(msg.getContent(), content);
    }

    @Test
    void getLastMessageTest() {
        service.publishMessage("first");
        service.publishMessage("second");
        service.publishMessage("third");
        String content = (String) service.getLastMessage().getContent();
        assertEquals("third", content);
    }

    @Test
    void getMessagesByPeriodTest() throws InterruptedException {
        ZonedDateTime start = ZonedDateTime.now();
        service.publishMessage("first");
        Thread.sleep(500);
        ZonedDateTime midl = ZonedDateTime.now();
        service.publishMessage("second");
        Thread.sleep(500);
        service.publishMessage("third");
        ZonedDateTime end = ZonedDateTime.now();
        int sizeOne = service.getMessagesByPeriod(midl, end).size();
        assertEquals(2,sizeOne);
        int sizeTwo = service.getMessagesByPeriod(start, end).size();
        assertEquals(3,sizeTwo);
    }
}