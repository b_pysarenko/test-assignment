curl -X POST 'http://localhost:18080/messages/publish' \
--header 'Content-Type: text/plain' \
--data-raw 'cpu temp=45,utilization=2'

curl -X GET -G 'http://localhost:18080/messages/getLast'

curl -X GET -G 'http://localhost:18080/messages/getByTime' \
-d 'start=2020-12-28%2021:40:20' \
-d 'end=2020-12-30%2022:32:06'
