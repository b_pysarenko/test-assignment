1. Create a local REDIS server on your machine (See https://redis.io/topics/quickstart  for the basic guidelines).

2. Create a JAVA Application based on REST API that communicates with the local REDIS server and meets the following requirements:
 - Be able to publish new messages to the REDIS server using a REST API GET request.
  >- Should be named “publish”.
  >- Should contain a “content” field with the new message content.
 - Be able to retrieve the last message that was on the REDIS server using a REST API GET request.
  >- Should be named “getLast”.
 - Be able to retrieve all messages that were on the REDIS and occurred between two given timestamps using a REST API POST request.
  >- Should be named “getByTime”.
  >- Should contain two parameters named “start” and “end” .

3. Create a new Git Repository and push your solution to this repository.
 - The repository should contain a README file with explanations on how to run and execute your work.

4. Extra points #1:
 - Wrap your solution with a Docker container -
  >- You can use docker-compose and separate between the Redis server and your Java application.

5. Extra points #2:
 - Use TDD methodologies as part of your solution

###Pay attention:
- You should not:
>- Copy a solution from an external resource. If we find the solution adequate, you'll need to defend it.
- We will evaluate:
>- Efficiency of your implementation
>- Code quality
>- Usage of OOP
>- Your abilities to defend the solution
- Assignment submission should include a link to the git repository with your code and instructions on how to run it.

