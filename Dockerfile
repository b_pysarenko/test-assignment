FROM openjdk:8-jdk-alpine

ARG JAR_NAME=skf.jar

ENV JAR_NAME ${JAR_NAME}
ENV JAVA_ENV -Xmx256m

ADD target/$JAR_NAME /

EXPOSE ${PORT}

ENTRYPOINT java $JAVA_ENV -jar /$JAR_NAME