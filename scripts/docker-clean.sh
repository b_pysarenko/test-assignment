#!/bin/bash
docker volume prune -f
docker network prune -f
docker rmi skf/rest:latest;
docker rmi redis:latest