#!/bin/bash
mvn install
scripts/docker-build.sh;
docker-compose -f scripts/docker-compose.yaml up -d --force-recreate;
