## DISCLAIMER ##
- In my opinion and according to best practice of RESTful approach,  writing data with GET requests and extracting data with POST requests are bad approach.
- Since there were no requirements for the "content" object I assume it like "String" but keep it as an Object. 

## README ##

### Summary ###
SKF test assignment

### Environment Requirements ###
Should be installed and configured:
- JDK 1.8
- maven 
- docker( + docker-compose )
- curl  

### How to run application ###
- Linux OS:
    - execute script ./app-run.sh
- Other OS:
    - in project folder execute `mvn install`
    - in project folder execute `docker build -t skf/rest:latest .`
    - in scripts folder execute `docker-compose up -d --force-recreate`
    
### How to test application ###
Add a new message:
>curl -X POST 'http://localhost:18080/messages/publish' \
--header 'Content-Type: text/plain' \
--data-raw 'cpu temp=45,utilization=2'

Get last message:
>curl -X GET -G 'http://localhost:18080/messages/getLast'

Get messages by time for period:
>curl -X GET -G 'http://localhost:18080/messages/getByTime' \
-d 'start=2020-12-28%2021:40:20' \
-d 'end=2020-12-30%2022:32:06'

Requests examples are placed in `app-curl.sh` file 

### How to stop application ###
- Linux OS:
    - execute script ./app-stop.sh
- Other OS:
    - in scripts folder execute `docker-compose down`
    - in project folder execute `docker volume prune -f`
    - in project folder execute `docker network prune -f`
    - in project folder execute `docker rmi skf/rest:latest`
    - in project folder execute `docker rmi redis:latest`
    